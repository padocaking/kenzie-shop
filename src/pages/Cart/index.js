import { Container } from './style'
import Header from '../../Components/Header'
import Footer from '../../Components/Footer'
import CartDisplay from '../../Components/CartDisplay'

const Cart = () => {
    return (
        <>
        <Header />
        <Container>
            <CartDisplay />
        </Container>
        <Footer />
        </>
    )
}

export default Cart