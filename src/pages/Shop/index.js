import { Container } from './style'
import Header from '../../Components/Header'
import Footer from '../../Components/Footer'
import ProductsDisplay from '../../Components/ProductsDisplay'

const Shop = () => {
    const products = [
        {name: "RTX 3080 Gaming X Trio 10G", price: "14899.90", image: "https://images9.kabum.com.br/produtos/fotos/127409/placa-de-video-msi-nvidia-geforce-rtx-3080-gaming-x-trio-10g-gddr6x_1599749901_g.jpg", category: "gpu"},
        {name: "GTX 1050 Ti OC 4GB", price: "1999.00", image: "https://images7.kabum.com.br/produtos/fotos/84137/84137_index_g.jpg", category: "gpu"},
        {name: "RX 6700 XT 12GB", price: "13998.00", image: "https://www.balaodainformatica.com.br/media/catalog/product/cache/1/image/365x365/9df78eab33525d08d6e5fb8d27136e95/1/1/11_155.jpg", category: "gpu"},
        {name: "RX 570 4GB", price: "2100.00", image: "https://http2.mlstatic.com/D_NQ_NP_994842-MLA32007146755_082019-O.jpg", category: "gpu"},
        {name: "Intel Core i7-11700", price: "2282.90", image: "https://img.terabyteshop.com.br/produto/g/processador-intel-core-i7-10700k-380ghz-470ghz-turbo-10-geracao-8-cores-16-threads-lga-1200-bx8070110700k_98005.jpg", category: "cpu"},
        {name: "Intel Core i3-10100F", price: "667.90", image: "https://img.terabyteshop.com.br/produto/g/processador-intel-core-i3-10100-360ghz-430ghz-turbo-10-geracao-4-cores-8-threads-lga-1200-bx8070110100_95282.png", category: "cpu"},
        {name: "AMD Ryzen 7 5800X", price: "2549.00", image: "https://media.pichau.com.br/media/catalog/product/cache/ef72d3c27864510e5d4c0ce69bade259/1/0/100-100000063wof_1.jpg", category: "cpu"},
        {name: "AMD Ryzen 5 5600X", price: "1662.90", image: "https://m.media-amazon.com/images/I/61vGQNUEsGL._AC_SX450_.jpg", category: "cpu"},
    ]

    return (
        <>
        <Header />
        <Container>
            <ProductsDisplay
                category="Placa de Vídeo"
                type="gpu"
                products={products}/>
            <ProductsDisplay
                category="Processador"
                type="cpu"
                products={products}/>
        </Container>
        <Footer />
        </>
    )
}

export default Shop