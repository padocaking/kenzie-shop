import { createStore, combineReducers } from 'redux'
import useReducer from './modules/products/reducer'

const reducers = combineReducers({ products: useReducer })
const store = createStore(reducers)

export default store