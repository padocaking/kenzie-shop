import { ADD_PRODUCT, REMOVE_PRODUCT } from './actionTypes'

const useReducer = (state = [], action) => {
    switch (action.type) {
        case ADD_PRODUCT:
            const { product } = action
            return [...state, product]
            
        case REMOVE_PRODUCT:
            const { removeProduct } = action
            return state.filter((product) => product.id !== removeProduct)

        default:
            return state
    }
}

export default useReducer