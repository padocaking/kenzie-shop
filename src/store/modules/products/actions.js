import { ADD_PRODUCT, REMOVE_PRODUCT } from './actionTypes'

export const addProduct = (product) => {
    return {
        type: ADD_PRODUCT,
        product,
    }
}

export const removeProduct = (removeProduct) => {
    return {
        type: REMOVE_PRODUCT,
        removeProduct,
    }
}