import styled from 'styled-components'

export const Container = styled.div`
width: 100%;
height: 4rem;
background-color: #003e84;
box-shadow: 0px 0px 10px 0px #7a7a7a;
color: white;
`

export const HeaderItems = styled.div`
display: flex;
justify-content: space-between;
align-items: center;
height: 100%;
max-width: 90%;
margin: auto;

h3 {
    font-size: 2.25rem;
    cursor: pointer;
}

nav {
    display: flex;

    div {
        font-size: 1.5rem;
        margin-left: 75px;
        cursor: pointer;
        display: flex;
        align-items: center;

        img {
            width: 30px;
            margin-right: 10px;
            filter: invert(1);
        }
    }
}

@media (max-width: 800px) {
    h3 {
        font-size: 1.15rem;
        width: 60px;
    }

    nav {

        div {
            font-size: 1rem;
            margin-left: 10px;

            img {
                width: 20px;
            }
        }
    }
}
`