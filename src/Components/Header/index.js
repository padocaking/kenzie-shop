import { Container, HeaderItems } from './style'
import { useHistory } from 'react-router'

const Header = () => {
    const history = useHistory()

    return (
        <Container>
            <HeaderItems>
                <h3 onClick={() => history.push("/")}>Kenzie Shop</h3>
                <nav>
                    <div onClick={() => history.push("/carrinho")}>
                        <picture>
                            <img src="https://image.flaticon.com/icons/png/512/263/263142.png" alt="Cart"/>
                        </picture>
                        <p>Carrinho</p>
                    </div>
                    <div>
                        <picture>
                            <img src="https://iconarchive.com/download/i91933/icons8/windows-8/User-Interface-Login.ico" alt="Sign In"/>
                        </picture>
                        <p>Entrar</p>
                    </div>
                </nav>
            </HeaderItems>
        </Container>
    )
}

export default Header