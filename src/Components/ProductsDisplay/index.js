import { Container, ProductsDiv } from './style'
import ProductCard from '../ProductCard'

const ProductsDisplay = ({ category, type, products }) => {
    return (
        <Container>
            <h2>{category}</h2>
            <ProductsDiv>
                <div>
                    {products.filter((product) => product.category === type).map((product) => (
                        <ProductCard
                            key={product.name}
                            image={product.image}
                            name={product.name}
                            price={product.price} />
                    ))}
                </div>
            </ProductsDiv>
        </Container>
    )
}

export default ProductsDisplay