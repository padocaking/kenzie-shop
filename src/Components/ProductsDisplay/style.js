import styled from 'styled-components'

export const Container = styled.div`
margin: auto;
display: flex;
flex-direction: column;
width: 1600px;
padding-top: 50px;

@media (max-width: 1600px) {
    width: 1300px;
}

@media (max-width: 1300px) {
    width: 95%;
}

h2 {
    font-size: 40px;
}
`

export const ProductsDiv = styled.div`

div {
    display: flex;
    flex-wrap: wrap;
    margin: 10px 10px 0px 0px;
}

@media (max-width: 800px) {
    overflow-x: scroll;

    div {
        flex-wrap: nowrap;
    }
}
`