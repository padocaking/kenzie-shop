import styled from 'styled-components'

export const Container = styled.div`
display: flex;
align-items: center;
justify-content: space-between;
border-bottom: 2px solid #eeeeee;

div {
    display: flex;
    align-items: center;

    .image {
    width: 150px;
    height: 150px;
    padding: 20px;
    }

    picture {
        background-color: #d3d3d3;
        width: 30px;
        margin-right: 30px;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 8px;
    }

    .remove {
        width: 30px;
        height: 30px;
        padding: 5px;
        border-radius: 8px;
        cursor: pointer;
    }

    h6 {
        font-size: 1.5rem;
        margin-left: 20px;
        font-weight: 400;
    }
}

span {
    font-size: 1.5rem;
    margin-right: 30px;
    color: green;
    font-weight: 700;
}
`