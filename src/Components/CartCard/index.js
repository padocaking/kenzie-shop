import { Container } from './style'
import { useDispatch } from 'react-redux'
import { removeProduct } from '../../store/modules/products/actions'

const CartCard = ({ image, name, price, id }) => {
    const dispatch = useDispatch()

    const handleClick = () => {
        dispatch(removeProduct(id))
    }

    return (
        <Container>
            <div>
                <img className="image" src={image} alt={name}/>
                <h6>{name}</h6>
            </div>
            <div>
                <span>R$ {price}</span>
                <picture>
                    <img
                        onClick={handleClick}
                        className="remove"
                        src="https://simpleicon.com/wp-content/uploads/trash.png"
                        alt="Remover"/>
                </picture>
            </div>
            
        </Container>
    )
}

export default CartCard