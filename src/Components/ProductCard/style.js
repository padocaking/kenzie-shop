import styled from 'styled-components'

export const Container = styled.div`
display: flex;
flex-direction: column;
justify-content: space-between;
width: 300px;
padding: 15px;
background-color: white;

img {
    width: 260px;
    height: 260px;
    margin-top: 10px;
    margin-bottom: 15px;
    border-bottom: 2px solid #eeeeee;
}

h4 {
    font-size: 20px;
    margin: 5px;
}

h3 {
    font-size: 30px;
    color: green;
    margin: 5px 0px 15px 5px;
}

button {
    width: 100%;
    height: 65px;
    border: none;
    background-color: #0c9aff;
    color: white;
    font-size: 20px;
    border-radius: 10px;
    transition: background-color ease 0.1s;
}

button:hover {
    background-color: #47b3ff;
}
`