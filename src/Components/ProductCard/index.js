import { Container } from './style'
import { useDispatch, useSelector } from 'react-redux'
import { addProduct } from '../../store/modules/products/actions'
import { toast } from 'react-toastify'

const ProductCard = ({ image, name, price }) => {
    const dispatch = useDispatch()
    const id = useSelector((state) => state.products.length)

    const product = {
        name: name,
        price: price,
        image: image,
        id: id,
    }

    const handleClick = () => {
        dispatch(addProduct(product))
        toast.success("Produto adicionado ao carrinho com sucesso!")
    }

    return (
        <Container>
            <img src={image} alt={name}/>
            <h4>{name}</h4>
            <h3>R$ {price}</h3>
            <button onClick={handleClick}>Adicionar ao carrinho</button>
        </Container>
    )
}

export default ProductCard