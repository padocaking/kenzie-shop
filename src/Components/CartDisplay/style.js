import styled from 'styled-components'

export const Container = styled.div`
margin: auto;
background-color: white;
width: 800px;
border-radius: 10px;
margin-top: 25px;
`

export const CartInfo = styled.div`
display: flex;
justify-content: space-between;
background-color: lightgray;
border-top-left-radius: 10px;
border-top-right-radius: 10px;

    h5 {
        padding: 10px;
        font-size: 1.5rem;
        margin: 10px 20px 10px 20px
    }
`

export const CartTotal = styled.div`

h6 {
    font-size: 1.5rem;
    padding: 20px 20px 0px 20px;
}

div {
    display: flex;
    justify-content: space-between;

    p {
        font-size: 1.5rem;
        padding: 20px;
    }

    span {
        font-size: 1.5rem;
        padding: 20px;
        color: green;
    }
}

.buttonDiv {
    display: flex;
    justify-content: center;

    button {
    width: 90%;
    height: 75px;
    background-color: #00a4f9;
    color: white;
    border: none;
    border-radius: 10px;
    margin: 25px;
    font-size: 1.2rem;
    transition: background-color 0.1s ease;
    }

    button:hover {
        background-color: #28b6ff;
    }
}

`