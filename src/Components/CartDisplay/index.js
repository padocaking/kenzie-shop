import { Container, CartInfo, CartTotal } from './style'
import { useSelector } from 'react-redux'
import CartCard from '../CartCard'

const CartDisplay = () => {
    const products = useSelector((state) => state.products)

    return (
        <Container>
            <CartInfo>
                <h5>Produtos:</h5>
                <h5>Preço:</h5>
            </CartInfo>
            {products.map((product) => (
                <CartCard
                    key={product.id}
                    image={product.image}
                    name={product.name}
                    price={product.price}
                    id={product.id}/>
            ))}
            <CartTotal>
                <div>
                    <h6>Resumo do pedido:</h6>
                    <h6>Total:</h6>
                </div>
                <div>
                    <p>{products.length} Produtos</p>
                    <span>R$ {products.reduce((a, b) => a + Number(b.price), 0).toFixed(2)}</span> 
                </div>
                <div className="buttonDiv">
                    <button>Escolher forma de pagamento</button>
                </div>
            </CartTotal>
        </Container>
    )
}

export default CartDisplay