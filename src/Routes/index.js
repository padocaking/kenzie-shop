import Shop from '../pages/Shop'
import Cart from '../pages/Cart'
import { Switch, Route } from 'react-router-dom'

const Routes = () => {
    return (
        <Switch>
            <Route exact path="/">
                <Shop />
            </Route>
            <Route path="/carrinho">
                <Cart />
            </Route>
        </Switch>
    )
}

export default Routes